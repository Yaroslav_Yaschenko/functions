const getSum = (str1, str2) => {
  if(typeof(str1)!=="string" ||typeof(str2)!=="string" ){
    return false;
  }
  let min;
  let max;
  if(str1.length<str2.length){
    min=str1.length;
    max=str2.length;
  }else{
    min=str2.length;
    max=str1.length;
  }
  if(str1==""){
    str1=0;
  }
  if(str2==""){
    str2=0;
  }
  if(isNaN(str1)||isNaN(str2)){
    return false;
  }
  var res=new Array();
  var isBig=false;
  for(let i=0;i<min;i++){
    let num1=Number(str1[str1.length-1-i]);
    let num2=Number(str2[str2.length-1-i]);
    if(isBig){
      num1++;
      isBig=false;
    }
    if(num1+num2<10){
      res.unshift(num1+num2);
      
    }else{
      res.unshift((num1+num2)%10);
      isBig=true;
    }
  }
  for(let i=max-1; i>=min;i--){
    if(str1.length>str2.length){
      res.unshift(Number(str1[i]));
    }else{
      res.unshift(Number(str2[i]));
    }
  }
  return res.join("");
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  var post=0;
  var comment=0;
  listOfPosts.forEach(element => {
    if(element.author==authorName){
      post++;
    }
    if(element.comments!=null&& element.comments.length!=0){
      element.comments.forEach(el=>{
        if(el.author==authorName){
          comment++;
        }
      });
    }
  });
  return "Post:"+post+",comments:"+comment;
};

const tickets=(people)=> {
  let money=0;
  for(element of people){
    if((Number(element)-25)>money){
      return "NO";
    }else{
      money+=25;
    }
  }
  return "YES";
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
